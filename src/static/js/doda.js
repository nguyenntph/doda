$(document).ready(function() {
	$('.menu-toggle').click(function(){
		if ($('.hidden-menu').css('display')==='none')
		{
			$('.visible-menu').slideToggle("slow",function(){
				$('.hidden-menu').fadeToggle("slow");
			});
			
		}
		else {
			$('.hidden-menu').fadeToggle("slow",function(){
				$('.visible-menu').slideToggle("slow");
			});
		}
	});
});
