var gulp = require('gulp'),
    swig = require('gulp-swig'),
    swig_configs = require('./swig-configs'),
    del = require('del'),
    connect = require('gulp-connect'),
    substituter = require('gulp-substituter'),
    substituter_configs = require('./substituter-configs');

var paths = {
    templates: 'src/**/*.html',
    scripts: 'src/static/js/**/*.js',
    styles: 'src/static/css/**/*.css',  
    images: 'src/static/img/**/*',
    fonts: 'src/static/fonts/**/*'
};

gulp.task('clean', function(cb) {
  del(['build'], cb);
});

gulp.task('connect', function() {
  connect.server({
    root: 'build',
    port: 80,
    host:"gulp.dev",
    livereload: true
  });
});

gulp.task('html', function () {
  gulp.src('build//**/*.html')
    .pipe(connect.reload());
});

gulp.task('templates', function() {
  return gulp.src(paths.templates)
    .pipe(swig(swig_configs))
    .pipe(substituter(substituter_configs))
    .pipe(gulp.dest('build/'))
});

gulp.task('styles', function() {
  return gulp.src(paths.styles)
    .pipe(gulp.dest('build/static/css'));
});

gulp.task('fonts', function() {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest('build/static/fonts'));
});

gulp.task('images', function() {
  return gulp.src(paths.images)
    .pipe(gulp.dest('build/static/img'));
});

gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
    .pipe(gulp.dest('build/static/js'));
});

gulp.task('watch', function() {  
    gulp.watch(['build/**/*.html'], ['html']);    
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.fonts, ['fonts']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.templates, ['templates']);
});

gulp.task('default', ['connect', 'watch', 'scripts', 'styles', 'fonts', 'images', 'templates']);
